#include "formula.hpp"

#include "catch.hpp"
#include <sstream>

inline std::istringstream _s( const char *str ) { return std::istringstream( str ); }

#define VALID_FORMULA(str) \
  CHECK_NOTHROW( bf::Formula ( _s( str ) ) )

#define INVALID_FORMULA(str) \
  CHECK_THROWS_AS( bf::Formula ( _s( str ) ), std::runtime_error )

#define EVAL_TRUE(formula, ...) \
  CHECK( bf::Formula ( _s( formula ) ).eval ( { \
       __VA_ARGS__ \
       } ) == true );

#define EVAL_FALSE(formula, ...) \
  CHECK( bf::Formula ( _s( formula ) ).eval ( { \
       __VA_ARGS__ \
       } ) == false );

TEST_CASE("Validity of formulas") {
  using namespace bf;

  VALID_FORMULA("!variable");
  VALID_FORMULA("a & b");
  INVALID_FORMULA("((a)");

}

TEST_CASE("Built-in operator semantics") {
  using namespace bf;

  CHECK( Formula( _s("a") ).eval( {
            { "a", true }
          } ) == true );

  EVAL_TRUE("a | a", { "a", true });
  EVAL_FALSE("a | a", { "a", false });

  EVAL_TRUE("!a", { "a", false });
  EVAL_FALSE("!a", { "a", true });
}
