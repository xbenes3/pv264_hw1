#pragma once

#include <vector>
#include <map>
#include <string>

namespace bf {

using Clause = std::vector< std::string >;

namespace detail {

} // namespace detail

class Formula {
public:
    explicit Formula( std::istream& s ) {
        // ToDo
    }

    // The constructor allows to create formula from temporary stringstream
    // without additional variable. Please leave unchanged.
    // R-value references (&&) will be explained later in the course
    Formula( std::istream&& s ) : Formula( s ) {}

    bool eval( const std::map< std::string, bool >& valuation ) const {
        // ToDo
    }

    std::vector< Clause > cnf() const {
        // ToDo
    }
};

} // namespace bf
