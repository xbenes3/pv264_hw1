cmake_minimum_required(VERSION 3.0)
project(formula)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++17 -Wall -Wextra -pedantic -Werror -Wold-style-cast")

add_executable(tests main.cpp student_test.cpp formula.cpp)
